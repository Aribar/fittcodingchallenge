This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Running Project
1. Go to https://www.npmjs.com/get-npm and follow instructions to download and update NPM.
1. Clone repository with your preferred tool or the following command.
	1. git clone https://Aribar@bitbucket.org/Aribar/fittcodingchallenge.git
1. Navigate to the local repo and install relevant packages with the following command:
	1. npm install

Afterwards the project should be ready to run. Just run the command below to start a development build.
* npm start
* Access the site at http://localhost:3000

You can start a production build by running the following commands:
* npm run build
	* You may need to install an additional package to serve this build:
	* npm install -g serve
* serve -s build
* Access the site at http://localhost:5000

### Credits
* Semantic UI for CSS styling: https://semantic-ui.com/