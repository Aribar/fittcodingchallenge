import React, { useState } from 'react';

export default function PageSelector(props) {
  const [currentPage, setCurrentPage] = useState(1);
  const pageLimit = 10; // Hard-coded due to API restriction.
  const pageCount = Math.ceil(props.count / pageLimit);
  const pages = [];

  for (let i = 1; i <= pageCount; i++) {
    pages.push(i);
  }

  function onHandleClick(event) {
    setCurrentPage(event.target.value);
    props.onPageChange(event.target.value);
  }

  return (
    <div align="center">
      {pages.map((num) => {
        if (num === currentPage) {
          return (
            <button key={num} className="mini ui active button" type="button" value={num} onClick={onHandleClick}>{num}</button>
          );
        }
        return (
          <button key={num} className="mini ui button" type="button" value={num} onClick={onHandleClick}>{num}</button>
        );
      })}
    </div>
  );
}
