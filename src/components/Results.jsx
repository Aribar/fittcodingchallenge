import React, { useState } from 'react';
import PageSelector from './PageSelector';

export default function Results(props) {
  const [sortField, setSortField] = useState('');
  const [sortOrder, setSortOrder] = useState('');
  const tableData = [...props.results];

  // Called when table header clicked to update sorting.
  function changeSort(field) {
    if (field === sortField) {
      if (sortOrder === 'ascending') setSortOrder('descending');
      if (sortOrder === 'descending') setSortOrder('ascending');
    } else {
      setSortField(field);
      setSortOrder('descending');
    }
  }

  // Used to sort the rows.
  function sortFunction(row1, row2) {
    if (sortOrder === 'ascending') {
      if (row1[sortField] > row2[sortField]) return -1;
      if (row1[sortField] < row2[sortField]) return 1;
    } else if (sortOrder === 'descending') {
      if (row1[sortField] > row2[sortField]) return 1;
      if (row1[sortField] < row2[sortField]) return -1;
    }
    return 0;
  }

  // Determines className for table headers.
  const orderClassName = (field) => {
    if (field === sortField) {
      if (sortOrder === 'ascending') return 'sorted ascending';
      if (sortOrder === 'descending') return 'sorted descending';
    }
    return '';
  };

  function onPageChange(page) {
    props.onPageChange(page);
    setSortField('');
    setSortOrder('');
  }


  return (
    <div>
      <table className="ui sortable celled striped table">
        <thead>
          <tr>
            <th className={orderClassName('name')} onClick={() => changeSort('name')}>Name</th>
            <th className={orderClassName('hair_color')} onClick={() => changeSort('hair_color')}>Hair Color</th>
            <th className={orderClassName('skin_color')} onClick={() => changeSort('skin_color')}>Skin Color</th>
            <th className={orderClassName('birth_year')} onClick={() => changeSort('birth_year')}>Birth Year</th>
            <th className={orderClassName('gender')} onClick={() => changeSort('gender')}>Gender</th>
          </tr>
        </thead>
        <tbody>
          {[...tableData].sort(sortFunction).map((person) => {
            return (
              <tr key={person.name}>
                <td>{person.name}</td>
                <td>{person.hair_color}</td>
                <td>{person.skin_color}</td>
                <td>{person.birth_year}</td>
                <td>{person.gender}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <PageSelector count={props.count} onPageChange={onPageChange} />
    </div>
  );
}
