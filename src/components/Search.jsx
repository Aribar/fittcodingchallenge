import React, { useState } from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';

export default function Search(props) {
  const [term, setTerm] = useState('');

  function onFormSubmit(event) {
    event.preventDefault();
    props.onTermSubmit(term);
  }

  function handleInputChange(event) {
    setTerm(event.target.value);
  }

  return (
    <div className={css(styles.searchContainer)}>
      <form onSubmit={onFormSubmit}>
        <label htmlFor="search" className={css(styles.label)}>Search:</label>
        <input name="search" type="text" placeholder="anakin" value={term} onChange={(handleInputChange)} />
        <button type="submit" value="submit">Enter!</button>
      </form>
    </div>
  );
}

const styles = StyleSheet.create({
  searchContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  label: {
    paddingRight: '12px',
  },
});
